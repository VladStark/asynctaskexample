package com.example.student1.potoki;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener  {

    ImageView iPicture;
    Button bLoad;
    ProgressBar pbLoading;
    String picUrl = "http://xn----7sbmatugdkfphym2m9a.xn--p1ai/wp-content/uploads/2015/02/ANDROID.png";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iPicture = (ImageView) findViewById(R.id.iv_image);
        bLoad = (Button) findViewById(R.id.b_load);
        bLoad.setOnClickListener(this);
        pbLoading =(ProgressBar) findViewById(R.id.pb_progress);
        pbLoading.setVisibility(View.GONE);

        /*
        subThread thread = new subThread();
        thread.start();
        Thread runnable = new Thread(new MyObject());
        runnable.start();
    */
    }

    @Override
    public void onClick(View view) {
        pbLoading.setVisibility(View.VISIBLE);
        ImageLoader task= new ImageLoader();
        task.execute(picUrl);
    }

    class ImageLoader extends AsyncTask<String,Void,Bitmap> {

        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap bitamp = BitmapFactory.decodeResource(getResources(),R.drawable.wrong);
            String urlText = strings[0];
            try {
                URL url = new URL(urlText);
                InputStream in = url.openStream();
                bitamp = BitmapFactory.decodeStream(in);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return bitamp;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            //super.onPostExecute(bitmap);
            iPicture.setImageBitmap(bitmap);
            pbLoading.setVisibility(View.GONE);
        }
    }
/*
    class subThread extends Thread {

        @Override
        public void run() {
            for(int i =0;i < 10;i++) {
                Log.d("mylog", "FROM sub thread");
                SystemClock.sleep(10);
            }
        }
    }


    class MyObject implements Runnable {

        @Override
        public void run() {
            for(int i =0;i < 10;i++){
                Log.d("mylog","from interface runnable");
                SystemClock.sleep(10);
            }

       }
    */
    }

